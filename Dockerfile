FROM skilgarriff/alpine-nginx

COPY public /var/www/html
#COPY default.conf /etc/nginx/conf.d/default.conf

# Create 'fabware' group whith gid 1000 and 'fabware' user in this group with uid 1000
#RUN \
#	addgroup -S -g 1000 fabware && \
#	adduser -D -S -u 1000 fabware && \
#	adduser fabware fabware

# Allow apache to run with 'fabware' user
# RUN chown -R fabware:fabware /usr/local/apache2/

# Start container as fabware
#USER fabware

#EXPOSE 8080